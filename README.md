#The Barrel - (c) Classic36 2015-2017
Version: Beta 4.5.1

Released: 8/3/17


This is the repo for reporting bugs which have occured on The Barrel Network.

* Please only use this repository to report bugs.
* Please do not submit duplicate bug reports. If needed, comment on an existing bug report requesting for it to be reopened along with additional info on how you came across the bug.
* Mark your bugs appropriately.
* If a bug has been marked as "WONT FIX", do not attempt to get the report reopened.